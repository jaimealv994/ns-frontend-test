/*import gulp from "gulp";
import panini from "panini";
import browserSync from "browser-sync";
import plumber from "gulp-plumber";
import sass from "gulp-sass";
import rename from "gulp-rename";
import cssnano from "cssnano";
import autoprefixer from "autoprefixer";
import postcss from "gulp-postcss";
import imagemin from "gulp-imagemin";
import newer from "gulp-newer";
import eslint from "gulp-eslint";
import uglify from "gulp-uglify";
import concat from "gulp-concat";*/



import gulp from 'gulp';
import del from 'del';
import sass from 'gulp-sass';
import babel from 'gulp-babel';
import htmlMin from 'gulp-htmlmin';
import html from 'gulp-html';
import eslint from 'gulp-eslint';
import uglify from 'gulp-uglify';
import imageMin from 'gulp-imagemin';
import browserSync from 'browser-sync';
import newer from 'gulp-newer';
import plumber from 'gulp-plumber';
import cssnano from 'cssnano';
import autoprefixer from 'autoprefixer';
import postcss from 'gulp-postcss';

const config = {
    html: {
        src: 'src/**/*.html',
    },
    scss: {
        src: 'src/styles/main.scss',
        dest: 'dist/styles'
    },
    img: {
        src: 'src/img/**/*',
        dest: 'dist/img'
    },
    scripts: {
        src: 'src/scripts/*.js'
    }
};

const processHtml = () => {
    return gulp.src(config.html.src)
        .pipe(html())
        .pipe(htmlMin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'));
};

const processSass = () => {
    return gulp.src(config.scss.src)
        .pipe(plumber())
        .pipe(sass({outputStyle: 'expanded'}))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(gulp.dest(config.scss.dest));
};

const lintJS = () => {
    return gulp
        .src(config.scripts.src)
        .pipe(plumber())
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
};


let transpileJSForProd = () => {
    return gulp.src('src/scripts/*.js')
        .pipe(babel())
        .pipe(uglify())
        .pipe(gulp.dest('dist/scripts'));
};

const processImages = () => {
    return gulp.src(config.img.src)
        .pipe(newer(config.img.dest))
        .pipe(imageMin())
        .pipe(gulp.dest(config.img.dest));
};

const serve = (done) => {
    browserSync.init({server: 'dist', port: 9000});
    done();
};

const watch = () => {
    gulp.watch('src/scripts/*.js', gulp.series(lintJS, transpileJSForProd)).on('change', browserSync.reload);
    gulp.watch('src/styles/**/*.scss', gulp.series(processSass)).on('change', browserSync.reload);
    gulp.watch('src/img/**/*', processImages).on('change', browserSync.reload);

    gulp.watch('dist/**/*.*').on('all', browserSync.reload);
    gulp.watch(config.html.src).on('all', gulp.series(processHtml, browserSync.reload));
};

gulp.task('build', gulp.parallel(processHtml, processSass, lintJS, transpileJSForProd, processImages));
gulp.task('serve', gulp.series('build', serve, watch));
